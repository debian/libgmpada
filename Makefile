#    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
#    Copyright (C) 2007 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#  Uncomment this and set this variable if you install by hand.
#  DESTDIR=

# gnatmake can do parallel builds; we don't want make to interfere.
.NOTPARALLEL:
CPUS := $(shell getconf _NPROCESSORS_ONLN)

export LIB_NAME := gmpada
SOVERSION := 1
export LIB_DIR := .
export SONAME := lib$(LIB_NAME).so.$(SOVERSION)

build: $(SONAME) lib$(LIB_NAME).a $(LIB_NAME).gpr

$(SONAME): gmp/gmp-constants.ads
	gnatmake -j$(CPUS) --create-missing-dirs -Pbuild.gpr \
          -XLIB_KIND=dynamic -XLIB_OBJ_DIR=shared-obj -XLIB_ALI_DIR=shared-ali

lib$(LIB_NAME).a: gmp/gmp-constants.ads
	gnatmake -j$(CPUS) --create-missing-dirs -Pbuild.gpr \
          -XLIB_KIND=static -XLIB_OBJ_DIR=static-obj -XLIB_ALI_DIR=static-ali

$(LIB_NAME).gpr: $(LIB_NAME).gpr.template
	sed -e "s/\$$(DESTDIR)/$(DESTDIR)/g" $< > $@

gmp/gmp-constants.ads: generate_constants
	./$< > $@

generate_constants: LDFLAGS += -lgmp

clean: gmp/gmp-constants.ads
# gnatclean needs to see this ads to remove its objects
	$(MAKE) -C demo $@
	rm -rf \
          $(LIB_NAME).gpr gmp/gmp-constants.ads generate_constants *~ gmp/*~ \
          shared-obj shared-ali static-obj static-ali \
          $(SONAME) lib$(LIB_NAME).so lib$(LIB_NAME).a

install: build
	install -d $(DESTDIR)/usr/share/ada/adainclude/$(LIB_NAME)
	install --mode=644 gmp/* $(DESTDIR)/usr/share/ada/adainclude/$(LIB_NAME)
	install --mode=644 $(LIB_NAME).gpr $(DESTDIR)/usr/share/ada/adainclude
	install -d $(DESTDIR)/usr/lib/ada/adalib/$(LIB_NAME)
	install --mode=444 shared-ali/* $(DESTDIR)/usr/lib/ada/adalib/$(LIB_NAME)
	install --mode=644 lib$(LIB_NAME).a $(SONAME) $(DESTDIR)/usr/lib
	cd $(DESTDIR)/usr/lib && ln -s $(SONAME) lib$(LIB_NAME).so

uninstall:
	rm -rf \
          $(DESTDIR)/usr/share/ada/adainclude/$(LIB_NAME) \
          $(DESTDIR)/usr/share/ada/adainclude/$(LIB_NAME).gpr \
          $(DESTDIR)/usr/lib/ada/adalib/$(LIB_NAME) \
          $(DESTDIR)/usr/lib/lib$(LIB_NAME).a $(DESTDIR)/usr/lib/$(SONAME) \
          $(DESTDIR)/usr/lib/lib$(LIB_NAME).so

test: gmp/gmp-constants.ads
	$(MAKE) -C demo $@

.PHONY: build clean install uninstall test


######################################################################
#  All that C stuff will be unnecessary with gprbuild’s mixed C/Ada
#  project files.  For the moment, gnatmake will embed all .o files,
#  we only have to compile them and store them in the object dir.
#####################################################################

C_OBJECTS := $(patsubst gmp/%.c,%.o,$(wildcard gmp/*.c))

$(SONAME): $(addprefix shared-obj/,$(C_OBJECTS))
lib$(LIB_NAME).a: $(addprefix static-obj/,$(C_OBJECTS))

%.o: CFLAGS += -g -O2
shared-obj/%.o: gmp/%.c | shared-obj
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ -fPIC
static-obj/%.o: gmp/%.c | static-obj
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@
shared-obj static-obj:
	mkdir --parents $@
