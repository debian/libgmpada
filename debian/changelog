libgmpada (1.6-3) experimental; urgency=medium

  * Build with gnat-14
  * Add comments to gbp.conf
  * Update Standards-Version and copyright years.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 02 Jan 2025 14:40:59 +0100

libgmpada (1.6-2) unstable; urgency=medium

  * Reupload to unstable for the gnat-13/time_t64 transition.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 17 Mar 2024 20:36:42 +0100

libgmpada (1.6-1) experimental; urgency=medium

  * New upstream release.
  * Upload to experimental because of the following transitions.
  * Build with gnat-13.  Rename library package with new SOversion.
  * Build with dh-ada-library/9. Remove version from -dev package name.
    Provide a name containing a hash.
    Build-Depend: gnat instead of gnat-13. Break/Replace previous -devs.
  * Use explicit debian/PACKAGE.{docs,examples} files and test
    dependencies now that the -dev package is unversioned.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 26 Dec 2023 20:38:38 +0000

libgmpada (1.5-7) unstable; urgency=medium

  * Break/Replace libgmpada*-dev since oldstable. Closes: #1034960.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 11 May 2023 00:39:45 +0200

libgmpada (1.5-6) unstable; urgency=medium

  * Mark the autopkgtest as flaky (#1026828 affects them on i386).

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 04 Jan 2023 14:11:32 +0000

libgmpada (1.5-5) unstable; urgency=medium

  * Disable post-build tests on i386. Ugly, but #1026828 (severity normal)
    should not force libgmpada out of the bookworm release.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 02 Jan 2023 16:10:45 +0000

libgmpada (1.5-4) unstable; urgency=medium

  * Downgrade the test sometimes failing after the build on i386
    (#1026828) to a non-critical message.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 31 Dec 2022 15:08:53 +0100

libgmpada (1.5-3) unstable; urgency=medium

  * Let -dev depend on libgmp-dev (previously done by dh-ada-library).

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 30 Dec 2022 18:53:13 +0000

libgmpada (1.5-2) unstable; urgency=medium

  * Reupload to unstable for the gnat-12 transition.
  * Standards-Version: 4.6.2 (no changes).
  * Explicitly install docs to -dev package.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 27 Dec 2022 02:39:16 +0100

libgmpada (1.5-1) experimental; urgency=medium

  * Build new upstream in experimental with gnat-12 and dh-ada-library 8.1.
    Rename -dev package but not shared library.
  * Standards-Version: 4.6.1

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 30 Aug 2022 10:04:13 +0000

libgmpada (1.4-4) unstable; urgency=medium

  * Reupload to unstable for the gnat-11 transition
  * Configure branch names for git-buildpackage
  * Require dh-ada-library 7.5, which generates ada:Depends differently

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 08 May 2022 18:22:26 +0200

libgmpada (1.4-3) experimental; urgency=medium

  * Rebuild with gnat-11. Rename -dev package accordingly.
  * Standards-Version 4.6.0.
  * Adapt to changes in debian_packaging.mk.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 13 Mar 2022 22:00:20 +0000

libgmpada (1.4-2) unstable; urgency=medium

  * Reupload to unstable for gnat-10 transition.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 13 Dec 2020 12:09:59 +0100

libgmpada (1.4-1) experimental; urgency=medium

  * Build with gnat-10. Rename lib and -dev package accordingly.
  * New upstream release.
  * Pass build flags via the environment
  * Refresh debhelper usage.
    No need to mention README.Debian in debian/docs anymore
    Drop obsolete Pre-Depends
  * Refresh dh-ada-library usage.
    Drop explicit dependency on libgmp-dev, now in ada:Depends.
    Move -lgmp from Library_Options to Linker_Options.

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 22 May 2020 00:30:12 +0000

libgmpada (1.3-2) unstable; urgency=medium

  * Reupload to unstable for gnat-9 transition. Closes: #944185.
  * Debhelper 12: dh-sequence syntax, examples are not compressed.
  * Standards-Version 4.5.0.
  * Drop redundant branch in Vcs-Git.
  * Drop explicit --as-needed, enabled by default by gcc-9.
  * Select specific dpkg-dev Makefile snippet.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 25 Feb 2020 11:53:20 +0100

libgmpada (1.3-1) experimental; urgency=medium

  * New upstream release, built with gnat-9.
    Migration in experimental, renaming lib and -dev packages.
  * Move Version Control System to salsa.
  * Standards-Version 4.4.0.
  * Rules-Requires-Root no.
  * Debhelper 12.
  * Remove unused signatures from upstream/signing-key.asc.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 18 Jul 2019 14:07:22 +0200

libgmpada (1.2-2) unstable; urgency=medium

  * Rebuild in unstable for gnat-8 transition.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 27 Oct 2018 15:09:55 +0200

libgmpada (1.2-1) experimental; urgency=medium

  * New upstream release. Rebuild with gnat-8, gcc-8.
    Rename shared library and -dev package per Debian Ada policy.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 08 May 2018 14:01:35 +0200

libgmpada (1.1-3) unstable; urgency=medium

  * Debhelper 11.
  * Drop obsolete version restriction on dpkg-dev.
  * Standards-Version 4.1.4.
  * Update URI syntax of VCS-Mtn field.
  * Disable tests if DEB_BUILD_OPTIONS contains nocheck.
  * Increase verbosity of run time test.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 03 May 2018 17:59:51 +0200

libgmpada (1.1-2) unstable; urgency=medium

  * Upload to unstable. Change versions: SO (runtime) and ALI (-dev).
  * Standards-Version: 4.0.1. Priority: optional.
  * Drop explicit rules targets confusing dh.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 09 Aug 2017 19:54:28 +0200

libgmpada (1.1-1) experimental; urgency=medium

  * New upstream built with gnat-7. No ALI/SO change in experimental.
    Drop dependency on mpfr and kfreebsd-amd64-gnat6-bug-in-test.diff.
  * Drop -pie from explicit hardening flags.
  * Standards-Version: 4.0.0. HTTPS version of copyright-format URL.
  * Debhelper 10.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 06 Jul 2017 12:56:40 +0200

libgmpada (1.0-3) unstable; urgency=medium

  * Previous patch does not fix the FTBFS and uses obsolete -aL option.
    Copy the objects in ALI directory instead.
  * Pass build flags to tests, especially activate assertions.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 16 May 2016 17:36:00 +0200

libgmpada (1.0-2) unstable; urgency=medium

  * patches/gnatmake-al-not-ao-alidir.diff: changing gnatmake options to
    work around a segfault in a test with gnat-6 on kfreebsd-amd64.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 16 May 2016 02:17:55 +0200

libgmpada (1.0-1) unstable; urgency=medium

  * New upstream release and website.
    Update watch, control/Homepage and copyright/Upstream-Contact.
    Drop get-orig-source target.
  * Build with gnat-6 and all hardening flags. Closes: #822775.
    Rename lib and -dev packages per Ada policy.
  * fake.gpr: new for dh-ada-library, no build project anymore.
  * Remove obsolete preinst scripts.
  * Replace manual -dbg packages with automatic -dbgsym.
  * Standards-Version: 3.9.8 (no changes).

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 27 Apr 2016 18:22:01 +0200

libgmpada (0.0.20131223-4) unstable; urgency=medium

  * Touch generated sources for deterministic ALI files and tar headers.
  * Standards-Version: 3.9.6 (no changes).

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 04 May 2015 14:47:22 +0200

libgmpada (0.0.20131223-3) unstable; urgency=medium

  * Pass CC to make, circumventing wrong autodetection. Closes: #768706.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 09 Nov 2014 14:18:38 +0100

libgmpada (0.0.20131223-2) unstable; urgency=medium

  * Migrate to gnat-4.9.
    Rename library and -dev package according to Ada policy.
  * move armored upstream signature to upstream/.
  * rules: run-tests target obsoleted by sadt from devscripts.

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 02 May 2014 04:21:56 +0200

libgmpada (0.0.20131223-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 3.9.4 (no changes).
  * watch, upstream-signing-key.pgp added.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 23 Dec 2013 05:40:19 +0100

libgmpada (0.0.20121109-3) unstable; urgency=low

  * tests: valid test names may not contain underscores.
  * rules: updated run-tests target.
  * remove any /usr/share/doc/pkg symlink, they cause maintenance problems.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 24 Aug 2013 00:16:17 +0200

libgmpada (0.0.20121109-2) unstable; urgency=low

  * tests/link_with_shared: avoid gnatmake printing on stderr.
    Closes: #702634, #703310.
    rules: run-tests target updated to emulate recent autopkgtest.
  * source/options: deleted, so that dpkg uses the default compression.
  * README.debian: why lintian hardening-no-fortify-functions may be ignored.
  * control: Build-Depends: gnat-4.6 >= 4.6.4, so that gnatlink handles
    --as-needed linker flag properly. Closes: #702632.
  * control: Standards-Version: 3.9.4.
  * Updated copyrights.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 10 Mar 2013 20:04:13 +0100

libgmpada (0.0.20121109-1) unstable; urgency=low

  * New upstream release (no patches needed anymore).
  * control: libgmpada3-dev -> libgmpada4-dev, with Breaks/Replaces,
    due to debian-ada-policy.
    XS-Testsuite: autopkgtest (Closes: #692663).
    Maintainer, DM-Upload-Allowed: I am a DD.
    Build-Depends: gprbuild is not needed anymore
  * copyright: GPL2+ -> GPL3+.
  * watch: gz -> bzip2.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 08 Nov 2012 16:17:00 +0100

libgmpada (0.0.20120331-2) unstable; urgency=low

  * Inherit hardening CPPFLAGS.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sat, 07 Jul 2012 21:37:49 +0200

libgmpada (0.0.20120331-1) unstable; urgency=low

  * New upstream release (neither ALI nor SO changes).
  * watch: search for bz2 tarballs too.
  * control: -dbg does not Pre-Depends: ${misc:Pre-Depends} for multiarch.
    -dev Depends: libmpfr-dev and libgmp-dev (Closes: #667481).
  * ada_libraries, control, rules: use dh_ada_library.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Wed, 04 Apr 2012 21:36:48 +0200

libgmpada (0.0.20120318-1) unstable; urgency=low

  * New upstream release (only build system changes).
  * README.Debian: replaced with a short reference to policy.
  * control: debhelper 9, Standards-Version 3.9.3 (no changes)
  * rules: simplified flags transmission
  * tests: create obj dir to conform to new upstream.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sun, 18 Mar 2012 06:18:31 +0100

libgmpada (0.0.20110925-3) unstable; urgency=low

  * control: remove dependencies on ada-compiler virtual package.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 09 Feb 2012 20:50:50 +0100

libgmpada (0.0.20110925-2) unstable; urgency=low

  * rules: do not strip static archive.
  * lintian-overrides: wildcard instead of an explicit architecture.
  * README.Debian: should not depend on the architecture.
  * rules: get-orig-source and run-tests targets.
  * control (Build-Depends): a gprbuild providing -R option.
  * rules: replaced ALDH subsystem by debhelper overrides for readability.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Fri, 09 Dec 2011 23:48:58 +0100

libgmpada (0.0.20110925-1) unstable; urgency=low

  * New upstream release, a patch removed.
  * Build with gcc and gnat 4.6. Closes: #642642, #633569.
  * Multi arch support for library and -dev package.
    Build-Depends: debhelper >= 8.9.8 to accept new *.ali directory.
  * Package renaming due to debian-ada-policy:
    libgmpada2-dev -> libgmpada3-dev
    libgmpada1 -> libgmpada2
    libgmpada1-dbg -> libgmpada-dbg
  * source/options: max bzip2 compression for debian.tar
  * source/local-options: abort-on-upstream-changes, unapply-patches

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sat, 01 Oct 2011 00:02:50 +0200

libgmpada (0.0.20100805-2) unstable; urgency=low

  * compat, control: updated to debhelper 8.
  * rules: made dh targets more generic
  * patches: LDLIBS instead of LDFLAGS (ubuntu bug770987)

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Wed, 13 Jul 2011 14:45:18 +0200

libgmpada (0.0.20100805-1) unstable; urgency=low

  * New upstream release.
  * control: (Build-Depends) upstream now uses gprbuild.
    An unversioned libgmp-dev is now available.
    (Standards-Version) 3.9.2 without changes.
    Development package renaming, Conflicts & Replaces due to debian-ada-policy.
  * copyright: updated.
  * patches: two integrated upstream, one updated.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Mon, 09 May 2011 00:25:14 +0200

libgmpada (0.0.20091124-4) unstable; urgency=low

  * control: (Architectures) any like all ada packages. Closes: #568441.
    see http://lists.debian.org/debian-ada/2010/02/msg00002.html)
    (Standards-Version) Updated to 3.9.0.0, no changes needed.
    (Vcs-Mtn) Corrected URL.
  * rules: SOVERSION and LIBNAME must be exported to upstream Makefile.
  * Switch to dpkg-source 3.0 (quilt) format.
  * (*-dev): depend on gnat, ada-compiler.
    (*-dbg): suggest gnat, gnat-4.4, ada-compiler.

  [Ludovic Brenta]
  * debian/control (DM-Upload-Allowed): yes, in anticipation of the
    application for DM status.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 01 Jul 2010 00:37:10 +0200

libgmpada (0.0.20091124-3) unstable; urgency=low

  * control: made each package description different to please lintian.
    (Architectures) explained why alpha is temporarily disabled.
  * patches: separated and documented removal of -gstabs+ and -fstack-check.
    Corrected an upstream incoherency in library project template.
  * watch: added.
  * rules: lintian override for shlib-with-executable-stack
    Generated separated debhelper files: more readability, less overloadings.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Wed, 20 Jan 2010 20:00:17 +0100

libgmpada (0.0.20091124-2) unstable; urgency=low

  * Patched demo/Makefile,devel.gpr,build.gpr: removed gcc flag -gstabs+
    in favor of more portable -g (FTBFS on ia64)
  * Closes: 559084 patched demo/Makefile to remove gcc flag -fstack-check
    Eric Botcazou says: "-fstack-check is broken with GCC 4.4 on x86/x86-64
    Linux, it generates code that will easily segfault"
    (http://gcc.gnu.org/PR20548).
  * rules;control;patches;README.source: added quilt support
  * control: XS-VCS- becomes Vcs- to please lintian
    (VCS-Mtn) new
    (VCS-Browser) official address
    (Architectures) any -> arch supporting GNAT to avoid dependency FTBFS

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Wed, 16 Dec 2009 00:04:29 +0100

libgmpada (0.0.20091124-1) unstable; urgency=low

  * New upstream version. Avoids collision with libgmp package.
  * control, rules: split -dev and -dbg packages.
  * docs, examples, rules: clarified, removed command embedding unwanted
    files, avoid compressing examples,
  * README.Debian: updated, conformed to other packages.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Tue, 24 Nov 2009 01:46:21 +0100

libgmpada (0.0.20091113-1) unstable; urgency=low

  * New package. Closes: #555682.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Fri, 13 Nov 2009 16:21:34 +0100
