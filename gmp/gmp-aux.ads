--    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
--    Copyright (C) 2007 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

with Interfaces.C; use Interfaces.C;
with GMP.Binding;  use GMP.Binding;

package GMP.Aux is

   --  Used for Generic_Text_IO.Put/Get procedures and
   --  Big_Float/Integer.Image/Value functions.

   procedure Put
     (Put_Character : access procedure (Item : in Character);
      Item          : in Mpz_T;
      Width         : in Natural;
      Base          : in Integer);

   procedure Put
     (Put_Character : access procedure (Item : in Character);
      Item          : in Mpq_T;
      Width         : in Natural;
      Base          : in Integer);

   procedure Put
     (Put_Character : access procedure (Item : in Character);
      Item          : in Mpf_T;
      Fore          : in Natural;
      Aft           : in Natural;
      Exp           : in Natural);

   function Is_Blank (Item : Character) return Boolean;

   function Is_Digit (Item : Character;
                      Base : int) return Boolean;

   generic
      Next : in out Character;
      with procedure Consume;
   package Generic_Scan is
      procedure Get_Mpz_T (Item  : in out Mpz_T;
                           Width : in     Natural);
      procedure Get_Mpf_T (Item  : in out Mpf_T;
                           Width : in     Natural);
   end Generic_Scan;

   --  Used for defining Text_IO as an instantiation of a generic.
   function To_Character (Item : Character) return Character;
   function To_Character (Item       : Character;
                          Substitute : Character := ' ')
                         return Character;

end GMP.Aux;
