//    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
//    Copyright (C) 2007 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <gmp.h>

int gmp_macro_mpz_cmp_si (mpz_t op1,
			       signed long int op2)
{
  return mpz_cmp_si (op1, op2);
}
int gmp_macro_mpz_cmp_ui (mpz_t op1,
			       unsigned long int op2)
{
  return mpz_cmp_ui (op1, op2);
}
int gmp_macro_mpz_sgn (mpz_t op)
{
  return mpz_sgn (op);
}
int gmp_macro_mpz_odd_p (mpz_t op)
{
  return mpz_odd_p (op);
}
int gmp_macro_mpz_even_p (mpz_t op)
{
  return mpz_even_p (op);
}
int gmp_macro_mpq_cmp_ui (mpq_t op1,
			       unsigned long int num2,
			       unsigned long int den2)
{
  return mpq_cmp_ui (op1, num2, den2);
}
int gmp_macro_mpq_cmp_si (mpq_t op1,
			       long int num2,
			       unsigned long int den2)
{
  return mpq_cmp_si (op1, num2, den2);
}
void* gmp_macro_mpq_numref (mpq_t op)
{
  return mpq_numref (op);
}
void* gmp_macro_mpq_denref (mpq_t op)
{
  return mpq_denref (op);
}
int gmp_macro_mpq_sgn (mpq_t op)
{
  return mpq_sgn (op);
}
int gmp_macro_mpz_kronecker (mpz_t a, mpz_t b)
{
  return mpz_kronecker (a, b);
}
int gmp_macro_mpf_sgn (mpf_t op)
{
  return mpf_sgn (op);
}
