--    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
--    Copyright (C) 2007 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

generic
   type Maybe_Character is private;
   type Maybe_String is array (Positive range <>) of Maybe_Character;
   type File_Type is limited private;
   type Positive_Count is range <>;
   with function To_Maybe_Character (Item : in Character) return Maybe_Character;

   with function To_Character (Item       : in Maybe_Character;
                               Substitute : in Character       := ' ')
                              return Character is <>;
   with function Current_Input  return File_Type is <>;
   with function Current_Output return File_Type is <>;
   with procedure Get (File : in  File_Type; Item : out Maybe_Character) is <>;
   with procedure Put (File : in File_Type; Item : in Maybe_Character) is <>;
   with procedure Look_Ahead (File        : in  File_Type;
                              Item        : out Maybe_Character;
                              End_Of_Line : out Boolean) is <>;
   with function End_Of_Line (File : in File_Type) return Boolean is <>;
   with function End_Of_Page (File : in File_Type) return Boolean is <>;
   with procedure Skip_Line (File    : in File_Type;
                             Spacing : in Positive_Count := 1) is <>;
   with procedure Skip_Page (File : in File_Type) is <>;
package GNU_Multiple_Precision.Generic_Text_IO is

   subtype Field is Natural;
   Max_IO_Length : Field := 1_000_000;

   --  Big_Integer and Big_Float have no maximum width.  So we adopt
   --  that convention: the Get procedures, when called with Width=>0
   --  has the same effect than with Width=>Max_IO_Length, except that
   --  leading blanks are not counted.

   subtype Number_Base is Integer range 2 .. 16;

   Default_Width : Field := 0;
   Default_Base : Number_Base := 10;

   procedure Get
     (File  : in     File_Type;
      Item  : in out Big_Integer;
      Width : in     Field       := 0);

   procedure Get
     (Item  : in out Big_Integer;
      Width : in     Field       := 0);

   procedure Put
     (File  : in File_Type;
      Item  : in Big_Integer;
      Width : in Field       := Default_Width;
      Base  : in Number_Base := Default_Base);

   procedure Put
     (Item  : in Big_Integer;
      Width : in Field       := Default_Width;
      Base  : in Number_Base := Default_Base);

   procedure Get
     (From : in     Maybe_String;
      Item : in out Big_Integer;
      Last :    out Positive);

   procedure Put
     (To   :    out Maybe_String;
      Item : in     Big_Integer;
      Base : in     Number_Base := Default_Base);

   procedure Put
     (File  : in File_Type;
      Item  : in Big_Rational;
      Width : in Field       := Default_Width;
      Base  : in Number_Base := Default_Base);

   procedure Put
     (Item  : in Big_Rational;
      Width : in Field       := Default_Width;
      Base  : in Number_Base := Default_Base);

   procedure Put
     (To   :    out Maybe_String;
      Item : in     Big_Rational;
      Base : in     Number_Base := Default_Base);

   Default_Fore : Field := 2;
   Default_Aft  : Field := Float'Digits - 1;
   Default_Exp  : Field := 3;

   procedure Get
     (File  : in     File_Type;
      Item  : in out Big_Float;
      Width : in     Field     := 0);

   procedure Get
     (Item  : in out Big_Float;
      Width : in     Field     := 0);

   procedure Put
     (File  : in File_Type;
      Item  : in Big_Float;
      Fore  : in     Field := Default_Fore;
      Aft   : in     Field := Default_Aft;
      Exp   : in     Field := Default_Exp);

   procedure Put
     (Item  : in Big_Float;
      Fore  : in     Field := Default_Fore;
      Aft   : in     Field := Default_Aft;
      Exp   : in     Field := Default_Exp);

   procedure Get
     (From : in     Maybe_String;
      Item : in out Big_Float;
      Last :    out Positive);

   procedure Put
     (To   :    out Maybe_String;
      Item : in     Big_Float;
      Aft  : in     Field        := Default_Aft;
      Exp  : in     Field        := Default_Exp);

end GNU_Multiple_Precision.Generic_Text_IO;
