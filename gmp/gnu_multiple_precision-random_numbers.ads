--    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
--    Copyright (C) 2007 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

package GNU_Multiple_Precision.Random_Numbers is

   type Generator is limited private;
   --  No need for Stream attributes for this type.
   --  Use Save, Reset and Big_Integer instead.

   procedure Random (Result : in out Big_Integer;
                     Gen    : in out Generator;
                     Modulo : in     Natural);
   --  For usual needs, the default Ada.Random is enough.
   procedure Random (Result : in out Big_Integer;
                     Gen    : in out Generator;
                     Modulo : in     Big_Integer);
   procedure Random_2exp (Result : in out Big_Integer;
                          Gen    : in out Generator;
                          Count  : in     Bit_Count);

   procedure Random_2exp (Result : in out Big_Float;
                          Gen    : in out Generator;
                          Count  : in     Bit_Count);
   --  0.0 <= Mantissa <1.0

   procedure Reset (Gen       : in out Generator;
                    Initiator : in     Integer);
   procedure Reset (Gen       : in out Generator;
                    Initiator : in     Big_Integer);
   procedure Reset (Gen : in out Generator);

   type State is private;
   --  No Stream attributes for this type.
   --  Use Big_Integer instead.

   procedure Save (Gen      : in     Generator;
                   To_State :    out State);
   procedure Reset (Gen        : in out Generator;
                    From_State : in     State);

   procedure Reset_Mersenne_Twister (Gen : in out Generator);
   procedure Reset_Linear_Congruential (Gen   : in out Generator;
                                        A     : in     Big_Integer;
                                        C     : in     Natural;
                                        M2exp : in     Bit_Count);
   procedure Reset_Linear_Congruential_2exp_Size (Gen            : in out Generator;
                                                  Size           : in     Natural;
                                                  Found_In_Table :    out Boolean);
   procedure Generate_Corner_Case_Random_Bytes
     (Rop   : in out Big_Integer;
      Gen   : in out Generator;
      Count : in     Bit_Count);
   --  Generate a random integer with long strings of zeros and ones
   --  in the binary representation.  Useful for testing functions and
   --  algorithms, since this kind of random numbers have proven to be
   --  more likely to trigger corner-case bugs.

private

   type State is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Gmp_Randstate_T;
   end record;

   procedure Initialize (Object : in out State);
   procedure Adjust     (Object : in out State);
   procedure Finalize   (Object : in out State);

   type Generator is limited record
      Gen_State : State;
   end record;

end GNU_Multiple_Precision.Random_Numbers;
