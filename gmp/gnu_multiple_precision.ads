--    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
--    Copyright (C) 2007 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

with Interfaces.C;
with GMP.Binding;
with Ada.Streams;
with Ada.Finalization;

package GNU_Multiple_Precision is

   subtype Bit_Count is Interfaces.C.unsigned_long;
   subtype A_Sign is Integer range -1 .. 1;

   type Big_Integer is private;
   type Big_Rational is private;
   type Big_Float is private;
   --     type Big_Float_Rounded is private;

private

   --  They will be needed for each body anyway...
   use Interfaces.C;
   use GMP.Binding;

   --  We declare all the types here, so that we can convert the
   --  private data from one type to another.

   --  Input, Output, Read and Write are handled by representation
   --  clauses.
--
   --  Equality must be overloaded in the same specification.

   type Big_Integer is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Mpz_T;
   end record;

   procedure Initialize (Object : in out Big_Integer);
   procedure Adjust     (Object : in out Big_Integer);
   procedure Finalize   (Object : in out Big_Integer);
   function "="  (Left, Right : Big_Integer) return Boolean;
   --  "/=" is automatically defined.

   --  Stream attributes are correctly inherited from Mpz_T, thanks to
   --  GNAT's black magic.

   type Big_Rational is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Mpq_T;
   end record;

   procedure Initialize (Object : in out Big_Rational);
   procedure Adjust     (Object : in out Big_Rational);
   procedure Finalize   (Object : in out Big_Rational);
   function "="  (Left, Right : Big_Rational) return Boolean;

   procedure Read (Stream : access Ada.Streams.Root_Stream_Type'Class;
                   Item   :   out  Big_Rational);
   for Big_Rational'Read use Read;

   procedure Write (Stream : access Ada.Streams.Root_Stream_Type'Class;
                    Item   : in     Big_Rational);
   for Big_Rational'Write use Write;

   type Big_Float is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Mpf_T;
   end record;

   procedure Initialize (Object : in out Big_Float);
   procedure Adjust     (Object : in out Big_Float);
   procedure Finalize   (Object : in out Big_Float);
   function "="  (Left, Right : Big_Float) return Boolean;

   procedure Read (Stream : access Ada.Streams.Root_Stream_Type'Class;
                   Item   :   out  Big_Float);
   procedure Write (Stream : access Ada.Streams.Root_Stream_Type'Class;
                    Item   : in     Big_Float);

   for Big_Float'Read use Read;
   for Big_Float'Write use Write;

   --     type Big_Float_Rounded is new Ada.Finalization.Controlled with record
   --        Value : GMP.Binding.Mpfr_T;
   --     end record;

   --     procedure Initialize (Object : in out Big_Float_Rounded);
   --     procedure Adjust     (Object : in out Big_Float_Rounded);
   --     procedure Finalize   (Object : in out Big_Float_Rounded);
   --     function "="  (Left, Right : Big_Float_Rounded) return Boolean;

   --     procedure Read (Stream : access Ada.Streams.Root_Stream_Type'Class;
   --                     Item   :   out  Big_Float_Rounded);
   --     procedure Write (Stream : access Ada.Streams.Root_Stream_Type'Class;
   --                      Item   : in     Big_Float_Rounded);

   --     for Big_Float_Rounded'Read use Read;
   --     for Big_Float_Rounded'Write use Write;

end GNU_Multiple_Precision;
