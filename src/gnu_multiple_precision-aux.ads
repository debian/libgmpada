--    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
--    Copyright (C) 2007-2010 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

with Interfaces.C; use Interfaces.C;
with GMP.Binding; use GMP.Binding;

private package GNU_Multiple_Precision.Aux is

   --  This private package isolates procedures used for
   --  Generic_Text_IO.Put/Get procedures and
   --  Big_Float/Integer.Image/Value functions.

   pragma Elaborate_Body;

   Unused_Character : constant Character := 'z';
   --  Used for end of input by get procedures.  It is also used as
   --  default replacement in conversion from wide_character to
   --  character.  It may raise a Data_Error that would have been
   --  undetected with a space.

   procedure Put
     (Put_Character : access procedure (Item : in Character);
      Item          : in Mpz_T;
      Width         : in Natural;
      Base          : in Integer);

   procedure Put
     (Put_Character : access procedure (Item : in Character);
      Item          : in Mpq_T;
      Width         : in Natural;
      Base          : in Integer);

   procedure Put
     (Put_Character : access procedure (Item : in Character);
      Item          : in Mpf_T;
      Fore          : in Natural;
      Aft           : in Natural;
      Exp           : in Natural);

   function Is_Blank (Item : Character) return Boolean;

   function Is_Digit (Item : Character;
                      Base : int) return Boolean;

   generic
      Next : in out Character;
      with procedure Consume;
   package Generic_Scan is
      procedure Get_Mpz_T (Item  : in out Mpz_T;
                           Width : in     Natural);
      procedure Get_Mpf_T (Item  : in out Mpf_T;
                           Width : in     Natural);
   end Generic_Scan;

private
   pragma Inline (Is_Blank);
   pragma Inline (Is_Digit);
end GNU_Multiple_Precision.Aux;
