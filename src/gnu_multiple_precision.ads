--    GMPAda, binding to the Ada Language for the GNU MultiPrecision library.
--    Copyright (C) 2007-2017 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

with Interfaces.C;
private with GMP.Binding;
private with Ada.Streams;
private with Ada.Finalization;

package GNU_Multiple_Precision is

   pragma Elaborate_Body;

   subtype Bit_Count is Interfaces.C.unsigned_long;
   subtype A_Sign is Integer range -1 .. 1;

   type Big_Integer is private;
   type Big_Rational is private;
   type Big_Float is private;

   --  Input, Output, Read and Write are handled by representation
   --  clauses.


   --  We declare all the types here, so that we can access the
   --  private data from one type to another in bodies (this is needed
   --  to call conversion fonctions).


   --  Used for defining Text_IO as an instantiation of a generic.
   function Identity (Item : in Character) return Character;
   function Identity (Item       : in Character;
                      Substitute : in Character := ' ')
                     return Character;
   --  Returns the provided Item unmodified.

private

   --  Equality must be overridden in the same specification.

   type Big_Integer is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Mpz_T;
   end record;

   overriding procedure Initialize (Object : in out Big_Integer);
   overriding procedure Adjust     (Object : in out Big_Integer);
   overriding procedure Finalize   (Object : in out Big_Integer);
   overriding function "=" (Left, Right : in Big_Integer) return Boolean;
   --  "/=" is automatically defined.

   --  Stream attributes are correctly inherited from Mpz_T, thanks to
   --  GNAT's black magic.

   type Big_Rational is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Mpq_T;
   end record;

   overriding procedure Initialize (Object : in out Big_Rational);
   overriding procedure Adjust     (Object : in out Big_Rational);
   overriding procedure Finalize   (Object : in out Big_Rational);
   overriding function "=" (Left, Right : Big_Rational) return Boolean;
   procedure Read (Stream : access Ada.Streams.Root_Stream_Type'Class;
                   Item   :   out  Big_Rational);
   for Big_Rational'Read use Read;
   procedure Write (Stream : access Ada.Streams.Root_Stream_Type'Class;
                    Item   : in     Big_Rational);
   for Big_Rational'Write use Write;

   type Big_Float is new Ada.Finalization.Controlled with record
      Value : GMP.Binding.Mpf_T;
   end record;
   overriding procedure Initialize (Object : in out Big_Float);
   overriding procedure Adjust     (Object : in out Big_Float);
   overriding procedure Finalize   (Object : in out Big_Float);
   overriding function "=" (Left, Right : Big_Float) return Boolean;

   pragma Inline ("=");
   pragma Inline (Adjust);
   pragma Inline (Finalize);
   pragma Inline (Identity);
   pragma Inline (Initialize);

end GNU_Multiple_Precision;
